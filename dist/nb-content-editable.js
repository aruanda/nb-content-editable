/*globals $:false*/
'use strict';

var React = require('react');

var nbContentEditable = React.createClass({
  getValue: function () {
    var element = $(this.refs.text);
    var value = this.props.onlyText ? element.text() : element.html();

    return value;
  },

  emitChange: function () {
    var cont = this;
    var notEvent = !cont.props.onChange;
    if (notEvent) return;

    var content = cont.getValue();
    cont.props.onChange(content);
  },

  render: function () {
    return React.createElement('span', {
      ref: 'text',
      onInput: this.emitChange,
      onBlur: this.emitChange,
      contentEditable: true,
      dangerouslySetInnerHTML: { __html: this.props.html }
    });
  }
});

module.exports = nbContentEditable;